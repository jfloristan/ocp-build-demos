## Demo deploying from Dockerfile
------------------------------

### Deploy application from Dockerfile

  Define ENV
  ```
  $ export APP=my-dockerfile-app
  $ export DIR=app
  $ export PROJECT=demo1-project
  ```
  
  Create project
  ```  
  $ oc new-project $PROJECT
  ```
  
  Create App
  ```
  $ oc new-app $DIR --name=$APP
  ```
  
  Check the objects created
  ```
  $ oc get all --selector app=$APP -o name
  ```

  Navigate the same in the Openshift Console
  
  Build App
  ```
  $ oc start-build $APP --from-dir $DIR --follow
  ```

  Check the again objects created
  ```
  $ oc get all --selector app=$APP -o name
  ```
  Expose Service
  ```
  $ oc expose service $APP
  ```

  Get Route and open it in a browser
  ```
  $ oc get route $APP
  ```

### Clean application

  ```
  $ oc delete all --selector app=$APP -o name
  $ oc delete project $PROJECT
  ```
