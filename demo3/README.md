## Demo deploying a scalable frontend and using HPA (autoscaling)
-----------------------------------------------------------------

### Deploy application from Dockerfile

  Define ENV
  ```
  $ export APP=my-autoscalable-app
  $ export DIR=app
  $ export PROJECT=demo3-project
  ```
  
  Create project
  ```  
  $ oc new-project $PROJECT
  ```
  
  Create App
  ```
  $ oc new-app $DIR --name=$APP
  ```
  
  Build App
  ```
  $ oc start-build $APP --from-dir $DIR --follow
  ```

  Expose Service
  ```
  $ oc expose service $APP
  ```

  Get Route and open it in a browser
  ```
  $ oc get route $APP
  ```

### Set CPU limits in the app to be able to saturate it
$ oc patch dc $APP -p '{"spec": {"template": {"spec": {"containers": [{"name": "my-autoscalable-app","resources": {"requests": {"cpu": "100m"},"limits": {"cpu": "100m"}}}]}}}}'

### Create the HPA (autoscale) configuration

$ oc autoscale dc/$APP --min 1 --max 10 --cpu-percent=20 

### Generate traffic to the APP

$ oc run -i --tty load-generator --image=busybox /bin/sh -l app=$APP

   ## In the terminal
   Hit enter for command prompt

   $ while true; do wget -q -O- <SCALABLE APP ROUTE>; done

### Clean application

  ```
  $ oc delete all --selector app=$APP -o name
  $ oc delete hpa $APP
  $ oc delete project $PROJECT
  ```
