<!-- toc -->

- [Deploy an application from a local dir using Dockerfile strategy](/demo1)
- [Deploy an application from a local dir using source strategy](/demo2) 
- [Deploy an application from a git repository using source strategy](/demo5) 
- [Deploy an application and configure autoscaling](/demo3) 
- [Blue - Green demo](/demo4)

<!-- tocstop -->
