## Demo deploying an application using blue-green deployment
------------------------------------------------------------

### Deploy the BLUE APP

  Define ENV
  ```
  $ export APP_REPO=https://gitlab.com/jfloristan/ocp-build-demos.git
  $ export APP=my-bg-app
  $ export BLUE_APP=blue-deploy
  $ export BLUE_BRANCH=master
  $ export GREEN_APP=green-deploy
  $ export GREEN_BRANCH=version2
  $ export PROJECT=demo4-project
  ```
  
  Create project
  ```  
  $ oc new-project $PROJECT
  ```
  
  Import wildfly image
  ```
  $ oc import-image wildfly --confirm \--from quay.io/wildfly/wildfly-centos7 --insecure -n openshift
  ```

  Create BLUE App
  ```
  $ oc new-app wildfly~$APP_REPO#$BLUE_BRANCH --context-dir=demo4/wildfly-basic --name=$BLUE_APP
  ```

  Expose the production service
  ```
  $ oc expose service $BLUE_APP --name=$APP
  ```

  Expose the BLUE Service for later testing
  ```
  $ oc expose service $BLUE_APP --name=$BLUE_APP
  ```

  Get Route and open it in a browser
  ```
  $ oc get route $APP
  ```

### Deploy the GREEN APP and switch traffic between BLUE and GREEN

  Create GREEN App
  ```
  $ oc new-app wildfly~$APP_REPO#$GREEN_BRANCH --context-dir=demo4/wildfly-basic --name=$GREEN_APP
  ```

  Expose the GREEN Service for later testing
  ```
  $ oc expose service $GREEN_APP --name=$GREEN_APP
  ```

  Patch the route to move traffic to GREEN and refresh the browser
  ```
  $ oc patch route/$APP -p '{"spec":{"to":{"name":"<GREEN_APP>"}}}'
  ```

  Patch the route to move traffic back to BLUE and refresh the browser
  ```
  $ oc patch route/$APP -p '{"spec":{"to":{"name":"<BLUE_APP>"}}}'
  ```

### Configure the route to weight traffic between BLUE and GREEN

  Configure route
  ```
  $ oc annotate route/$APP haproxy.router.openshift.io/balance=roundrobin
  $ oc annotate route/$APP haproxy.router.openshift.io/disable_cookies=true
  ```

  Update traffic split 50/50
  ```
  $ oc set route-backends $APP $BLUE_APP=50 $GREEN_APP=50
  ```

### Clean application

  ```
  $ oc delete all --selector app=$BLUE_APP -o name
  $ oc delete all --selector app=$GREEN_APP -o name
  $ oc delete project $PROJECT
  ```
